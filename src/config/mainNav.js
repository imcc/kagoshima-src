// Helper
import {staticScrollTo} from '../helper/scroll';
import sections from './sections';

export const navItems = Object.keys(sections).map(key => {
    return {
        key,
        text: sections[key],
    };
});
