// Images
import img01 from '../../assets/images/SectionIntro/volcano-full.jpg';
import img02 from '../../assets/images/SectionIntro/jingu-full.jpg';
import img03 from '../../assets/images/SectionGallery/01-1920w.jpg';
import img04 from '../../assets/images/SectionGallery/02-1920w.jpg';
import img05 from '../../assets/images/SectionGallery/03-1920w.jpg';
import img06 from '../../assets/images/SectionGallery/04-1920w.jpg';
import img07 from '../../assets/images/SectionGallery/05-1920w.jpg';
import img08 from '../../assets/images/SectionGallery/06-1920w.jpg';
import img09 from '../../assets/images/SectionGallery/07-1920w.jpg';
import img10 from '../../assets/images/SectionGallery/08-1920w.jpg';

export default [
    img01,
    img02,
    img03,
    img04,
    img05,
    img06,
    img07,
    img08,
    img09,
    img10,
];
