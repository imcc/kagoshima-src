export default {
    main: '首頁',
    intro: '簡介',
    gallery: '圖集',
    travels: '遊歷',
    making: '製作',
    aboutMe: '關於我',
};
