import React from 'react';
import PropTypes from 'prop-types';
import {Provider} from 'react-redux';
import {withRouter, Switch, Route, Redirect} from 'react-router-dom';

// Store
import store from './store/Main';

// Component
import SectionNav from './reduxComponent/SectionNav';
import SectionMain from './reduxComponent/SectionMain';
import SectionIntro from './reduxComponent/SectionIntro';
import SectionGallery from './reduxComponent/SectionGallery';
import SectionMaking from './reduxComponent/SectionMaking';
import SectionAboutMe from './reduxComponent/SectionAboutMe';
import SectionTravels from './reduxComponent/SectionTravels';
import SectionFooter from './component/SectionFooter';
import ImagesLightBox from './reduxComponent/ImagesLightBox';
import LoadingItem from './reduxComponent/LoadingItem';
import ArticleReader from './component/ArticleReader';
import FullscreenMenuList from './reduxComponent/FullscreenMenuList';

// Helper
import FontFaceObserver from 'fontfaceobserver';
import {preloadImages} from './helper/preload';
import {setAppShowModal, whichSection, staticScrollTo} from './helper/scroll';
import {updateURLBySectionId} from './helper/url';

// Action
import * as loadingItemActions from './action/LoadingItem';
import * as imagesLightBoxActions from './action/ImagesLightBox';
import * as appActions from './action/App';
import {updateMini as updateSectionNavMini} from './action/SectionNav';
import {updateLoaded as updateMainLoaded} from './action/SectionMain';
import {updateLoaded as updateIntroLoaded} from './action/SectionIntro';
import {updateLoaded as updateGalleryLoaded, } from './action/SectionGallery';
import {updateLoaded as updateTravelsLoaded} from './action/SectionTravels';
import {updateLoaded as updateMakingLoaded} from './action/SectionMaking';
import {updateLoaded as updateAboutMeLoaded} from './action/SectionAboutMe';

// Config
import travelsArticles from './config/articles';
import galleryImages from './config/galleryImages';

const {dispatch, getState} = store;
let scrollTimer = null;
let scrolling = false;

class App extends React.Component {
    static defaultProps = {
        history: {},
        match: {},
    };

    static propTypes = {
        history: PropTypes.object.isRequired,
        match: PropTypes.object.isRequired,
    };

    componentDidMount () {
        const {match} = this.props;

        // Preload images for lightbox
        preloadImages(galleryImages)
        .then(() => store.dispatch(imagesLightBoxActions.updatePreloaded(true)));

        // Disable last scrolled position in history state
        if (window.history.scrollRestoration) {
            window.history.scrollRestoration = 'manual';
        }

        dispatch(appActions.updateInitialized(true));
        dispatch(updateMainLoaded(true));

        // Update Scroll position
        staticScrollTo(match.params.section);

        window.addEventListener('scroll', () => {
            scrolling = true;
            scrollTimer = setTimeout(() => {
                if (scrolling) {
                    scrolling = false;

                    this.onWindowScrollHandle();
                    clearTimeout(scrollTimer);
                }
            }, 200);
        });
    }

    componentDidUpdate (prevProps) {
        const {
            history: {action},
            match: {params: {section}, isExact}
        } = this.props;

        if (isExact && action !== 'REPLACE' && section !== prevProps.section) {
            // Update Scroll position
            staticScrollTo(section);
        }
    }

    onWindowScrollHandle () {
        const {initialized} = getState().App;
        const {history} = this.props;

        if (!initialized) return;

        const sectionScrollActions = [
            updateMainLoaded,
            updateIntroLoaded,
            updateGalleryLoaded,
            updateTravelsLoaded,
            updateMakingLoaded,
            updateAboutMeLoaded,
        ];
        const currentSectionIdx = whichSection();
        const shouldNavBeMini = currentSectionIdx > -1 && currentSectionIdx !== 0;

        if (currentSectionIdx < 0 || !sectionScrollActions[currentSectionIdx]) return;

        updateURLBySectionId(currentSectionIdx, history);

        store.dispatch(sectionScrollActions[currentSectionIdx](true));
        store.dispatch(updateSectionNavMini(shouldNavBeMini));
    }

    render () {
        return (
            <Provider store={store}>
                <div>
                    <SectionNav />
                    <SectionMain />
                    <SectionIntro />
                    <SectionGallery />
                    <SectionTravels />
                    <SectionMaking />
                    <SectionAboutMe />
                    <SectionFooter />
                    <FullscreenMenuList />
                    <Switch>
                        <Route
                            exact
                            path="/gallery/:id"
                            render={({match, history}) => {
                                const {id} = match.params;

                                setAppShowModal(true);

                                return (<ImagesLightBox
                                    data={galleryImages}
                                    currentId={+id}
                                    onCurrentIdChange={currentId => {
                                        history.push(`/gallery/${currentId}`);
                                    }}
                                />);
                            }}>
                        </Route>
                    </Switch>
                    <Switch>
                        <Route
                            exact
                            path="/travels/:id"
                            render={({match}) => {
                                const {id} = match.params;

                                return travelsArticles[+id] ?
                                    <ArticleReader currentId={+id} /> :
                                    <Redirect to="/travels" />
                            }}>
                        </Route>
                    </Switch>
                </div>
            </Provider>
        );
    }
};

export default withRouter(App);
