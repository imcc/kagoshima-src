import {createStore} from 'redux';
import reducer from '../reducer/Main';

export default createStore(reducer);
