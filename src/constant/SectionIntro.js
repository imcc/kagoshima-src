export const UPDATE_CURRENT_ID = Symbol();
export const UPDATE_SCROLL_OFFSET = Symbol();
export const UPDATE_LOADED = Symbol();
