export const UPDATE_CURRENT_ID = Symbol();
export const UPDATE_SCROLL_OFFSET = Symbol();
export const UPDATE_VISIBLE = Symbol();
export const UPDATE_OFFSCREEN = Symbol();
export const UPDATE_INSTANT_SCROLL = Symbol();
export const UPDATE_PRELOADED = Symbol();
