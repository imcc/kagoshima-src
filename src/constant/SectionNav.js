export const UPDATE_CURRENT_ID = Symbol();
export const UPDATE_MINI = Symbol();
export const UPDATE_VISIBLE = Symbol();
export const UPDATE_OFFSCREEN = Symbol();
export const SCROLL_VISIBILITY_CHANGE_INTERVAL = 200;
