// css imports
import '../assets/css/kagoshima/core.scss';

// javascript
import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import {HashRouter, Switch, Route, Redirect} from 'react-router-dom';

import sectionsConfig from './config/sections';

import App from './App';

const root = document.getElementById('kagoshima');


const index = (
    <HashRouter>
          <Switch>
              <Route
                  path="/:section"
                  children={({match}) => {
                      const isValidSection = (section) => {
                          return Object.keys(sectionsConfig).includes(section);
                      };

                      return isValidSection(match.params.section) ?
                        (
                            <AppContainer>
                                <App />
                            </AppContainer>
                        ) :
                        (
                            <Redirect to="/main" />
                        )
                      ;
                  }}
              />
              <Route>
                   <Redirect to="/main" />
              </Route>
          </Switch>
    </HashRouter>
);

ReactDOM.render(index, root);

if (module.hot) {
    module.hot.accept('./App', () => {
        ReactDOM.render(index, root);
    })
}
