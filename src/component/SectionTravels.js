import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import {withRouter} from 'react-router-dom';

// Component
import ShiftingSquares from './ShiftingSquares';

// SCSS
import styles from '../../assets/css/kagoshima/component/_SectionTravels.scss';

// Article
import articles from '../config/articles';

class SectionTravels extends React.Component {
    static defaultProps = {
        loaded: false,
    };

    static propTypes = {
        loaded: PropTypes.bool.isRequired,
        history: PropTypes.object.isRequired,
    };

    onShowArticleReader (idx) {
        const {history} = this.props;

        history.push(`/travels/${idx}`);
    }

    render() {
        const cn = classnames.bind(styles);
        const {loaded} = this.props;
        const articleSections = articles.map((item, idx) => {
            const firstP = idx ? '' : item.text.split('\n')[0].slice(0, 120) + '...';

            return (
                <section key={idx} className={cn('articlePreview')}>
                    <figure>
                        <img srcSet={`${item.image.smartPhone} 480w, ${item.image.tablet} 768w`}
                            src={item.image.desktop}
                            alt={item.title}
                            onClick={() => this.onShowArticleReader(idx)}
                        />
                    </figure>
                    <header>
                        <h1 onClick={() => this.onShowArticleReader(idx)}>
                            <b>{item.title}</b>
                        </h1>
                        <h4>2017-09-15 | 黃耀宏</h4>
                    </header>
                    <p>
                        <span>{firstP}</span>
                        <span onClick={() => this.onShowArticleReader(idx)}>[繼續閱讀]</span>
                    </p>
                </section>
            );
        });

        return (
            <section className={classnames(cn('main'), loaded ? 'loaded' : 'ready-to-load')} id="travels">
                <h1>遊歷</h1>
                <div>
                    {articleSections[0]}
                    <div>
                        {[articleSections[1], articleSections[2]]}
                    </div>
                </div>
            </section>
        );
    }
};

export default withRouter(SectionTravels);
