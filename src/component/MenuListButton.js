import React from 'react'
import classnames from 'classnames/bind';

// SCSS
import styles from "../../assets/css/kagoshima/component/_MenuListButton.scss";

export default class MenuListButton extends React.Component {
    static defaultProps = {
        checked: false,
    };

    render() {
        const cn = classnames.bind(styles);
        const {checked, onClick} = this.props;

        return (
            <div className={cn('main')} onClick={e => onClick(e)}>
                <div data-checked={checked}>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        );
    }
};
