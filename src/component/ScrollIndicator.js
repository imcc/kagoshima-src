import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames/bind';

// SCSS
import styles from '../../assets/css/kagoshima/component/_ScrollIndicator.scss';

export default class ScrollIndicator extends React.Component {
    static defaultProps = {
        count: 0,
        currentId: 0,
    };

    static propsTypes = {
        count: PropTypes.number.isRequired,
        currentId: PropTypes.number.isRequired,
        onCurrentIdChange: PropTypes.func.isRequired,
    };

    render() {
        const cn = classnames.bind(styles);
        const {count, currentId, onCurrentIdChange} = this.props;
        const itemsEl = [];

        for(let i = 0; i < count; i += 1) {
            itemsEl.push(
                <li
                    key={i}
                    data-selected={i === currentId}
                    onClick={() => onCurrentIdChange(i)}
                ></li>
            );
        }

        return (
            <ul className={cn('main')}>{itemsEl}</ul>
        );
    }
};
