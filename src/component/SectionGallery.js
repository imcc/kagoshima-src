import React from 'react'
import classnames from 'classnames/bind';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

// Component
import HScrollingItem from './HScrollingItem';
import ScrollIndicator from './ScrollIndicator';

// SCSS

import styles from '../../assets/css/kagoshima/component/_SectionGallery.scss';

export default class SectionGallery extends React.Component {
    static defaultProps = {
        loaded: false,
    };

    static propTypes = {
        loaded: PropTypes.bool.isRequired,
        onLightBoxShow: PropTypes.func.isRequired,
    };

    render() {
        const cn = classnames.bind(styles);
        const {loaded, onLightBoxShow} = this.props;
        const imageItems = Array(8).fill(null).map((item, idx) => {
            return (
                <li key={idx}>
                    <div className={cn('thumbnail')}>
                        <Link to={`/gallery/${idx + 2}`} className={classnames('blockedAnchor')}>
                            <div className={cn('magnifier')}></div>
                        </Link>
                    </div>
                </li>
            );
        });

        return (
            <section className={classnames(cn('main'), loaded ? 'loaded' : 'ready-to-load')} id="gallery">
                <h1>圖集</h1>
                <div>
                    <ul className={classnames(cn('imageGrid'), loaded ? 'loaded' : 'ready-to-load')}>
                        {imageItems}
                    </ul>
                </div>
            </section>
        );
    }
};
