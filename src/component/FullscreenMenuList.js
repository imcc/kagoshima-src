import React from 'react'
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import {NavLink} from 'react-router-dom';

// SCSS
import styles from "../../assets/css/kagoshima/component/_FullscreenMenuList.scss";

// Helper
import {setAppShowModal} from '../helper/scroll';

// Config
import {navItems} from '../config/mainNav';

export default class FullscreenMenuList extends React.Component {
    static defaultProps = {
        visible: false,
        offscreen: true,
    };

    static propTypes = {
        visible: PropTypes.bool.isRequired,
        offscreen: PropTypes.bool.isRequired,
        onVisibleChange: PropTypes.func.isRequired,
        onOffscreenChange: PropTypes.func.isRequired,
    };

    componentWillUpdate (nextProps) {
        setAppShowModal(nextProps.expand);
    }

    onTransitionEndHandle () {
        const {visible, offscreen, onOffscreenChange} = this.props;

        if (!visible && !offscreen) {
            return onOffscreenChange(true);
        }
    }

    render() {
        const cn = classnames.bind(styles);
        const {visible, offscreen, onVisibleChange} = this.props;
        const itemsEl = navItems.map((data, idx) => {
            const {key, text} = data;

            return (
                <li
                    key={idx}
                    onClick={e => {
                        onVisibleChange(false);
                    }}
                >
                    <NavLink
                        className={classnames('blockedAnchor')}
                        to={`/${key}`}
                    >{text}</NavLink>
                </li>
            );
        });

        return (
            <div
                className={classnames(visible ? 'visible' : 'invisible', offscreen ? 'offscreen' : 'onscreen', cn('main'))}
                onTransitionEnd={() => this.onTransitionEndHandle()}
            >
                <ul>{itemsEl}</ul>
            </div>
        );
    }
};
