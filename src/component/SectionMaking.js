import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';

// Component
import ShiftingSquares from './ShiftingSquares';

// SCSS

import styles from '../../assets/css/kagoshima/component/_SectionMaking.scss';

export default class SectionMaking extends React.Component {
    static defaultProps = {
        loaded: false,
    };

    static propTypes = {
        loaded: PropTypes.bool.isRequired,
    };

    render() {
        const cn = classnames.bind(styles);
        const {loaded} = this.props;

        return (
            <section className={classnames(cn('main'), loaded ? 'loaded' : 'ready-to-load')} id="making">
                <h1>製作</h1>
                <div>
                    <div>
                        <div>
                            <ShiftingSquares />
                        </div>
                    </div>
                    <div>
                        <p>
                            使用React作為View的呈現框架，另外搭配React Redux以及React Router進行資料流與路由管理。
                        </p>
                        <p>
                            全頁主要以ES6為撰寫語言，輔以Webpack與Babel進行轉譯、壓縮、與打包。
                        </p>
                        <p>
                            使用SCSS作為CSS的Pre-processor，頁面中主要動畫效果皆使用CSS3完成。
                        </p>
                        <p>
                            以語義化的方式呈現頁面，以期取得最佳的SEO優化、頁面載入、以及最大的瀏覽器相容。
                        </p>
                        <p>
                            使用Git作為功能開發與版本管理，目前專案放置於Bitbucket上，點擊<a href="https://bitbucket.org/imcc/kagoshima-src/">此處前往</a>。
                        </p>
                    </div>
                </div>
            </section>
        );
    }
};
