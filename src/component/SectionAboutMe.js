import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';

// Component
import ShiftingSquares from './ShiftingSquares';

// SCSS

import styles from '../../assets/css/kagoshima/component/_SectionAboutMe.scss';

export default class SectionAboutMe extends React.Component {
    static defaultProps = {
        loaded: false,
    };

    static propTypes = {
        loaded: PropTypes.bool.isRequired,
    };

    render() {
        const cn = classnames.bind(styles);
        const {loaded} = this.props;

        return (
            <section className={classnames(cn('main'), loaded ? 'loaded' : 'ready-to-load')} id="aboutMe">
                <h1>關於我</h1>
                <div>
                    <div>
                        <div></div>
                        <ul>
                            <li>黃耀宏</li>
                            <li>David Huang</li>
                        </ul>
                        <div onClick={() => window.location = 'mailto: cl6.6ba@gmail.com'}>
                            <div></div>
                            <div>電子郵件</div>
                        </div>
                    </div>
                    <div>
                        <ul>
                            <li>
                                <span>最高學歷：</span>
                                <span>中央大學 英美語文學系畢業</span>
                            </li>
                            <li>
                                <span>工作經歷：</span>
                                <ul className={cn('careerList')}>
                                    <li>
                                        <div>2012-06 ~ 2013-07</div>
                                        <div>3D角色動作師(金奇映畫)</div>
                                    </li>
                                    <li>
                                        <div>2013-07 ~ 2014-03</div>
                                        <div>3D角色動作師(壹動畫)</div>
                                    </li>
                                    <li>
                                        <div>2014-06 ~ 2017-08</div>
                                        <div>網路開發工程師(萬里科技)</div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <span>使用語言：</span>
                                <ul className={cn('languageList')}>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        );
    }
};
