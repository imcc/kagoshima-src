import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';

// Component
import HScrolllingItem from './HScrollingItem';

// SCSS
import styles from '../../assets/css/kagoshima/component/_LightBox.scss';

export default class LightBox extends React.Component {
    static defaultProps = {
        currentId: 0,
        scrollOffset: 0,
        visible: false,
        // offscreen: true,
    };

    static propTypes = {
        currentId: PropTypes.number.isRequired,
        onCurrentIdChange: PropTypes.func.isRequired,
        scrollOffset: PropTypes.number.isRequired,
        onScrollOffsetChange: PropTypes.func.isRequired,
        children: PropTypes.array.isRequired,
        visible: PropTypes.bool.isRequired,
        onVisibleChange: PropTypes.func.isRequired,
        instantScroll: PropTypes.bool.isRequired,
        onInstantScrollChange: PropTypes.func.isRequired,
        onClose: PropTypes.func.isRequired,
    };

    onTransitionEndHandle () {
        const {visible, onClose} = this.props;

        if (!visible) onClose();
    }

    render() {
        const cn = classnames.bind(styles);
        const {
            currentId, onCurrentIdChange, scrollOffset, onScrollOffsetChange,
            visible, onVisibleChange, children,
            instantScroll, onInstantScrollChange,
        } = this.props;

        return (
            <div
                ref="main"
                className={classnames(visible ? 'visible' : 'invisible', cn('main'))}
                onTransitionEnd={() => this.onTransitionEndHandle()}
            >
                <div className={cn('scrollContainer')}>
                    <HScrolllingItem
                        children={children}
                        currentId={currentId}
                        onCurrentIdChange={currentId => onCurrentIdChange(currentId)}
                        scrollOffset={scrollOffset}
                        onScrollOffsetChange={scrollOffset => onScrollOffsetChange(scrollOffset)}
                        instantScroll={instantScroll}
                        onInstantScrollChange={instantScroll => onInstantScrollChange(instantScroll)}
                    />
                </div>
                <div
                    className={cn('closeBtn')}
                    onClick={() => onVisibleChange(false)}
                >←上一頁</div>
                <div
                    className={cn('leftBtn')}
                    onClick={() => onCurrentIdChange(currentId - 1)}
                >&#60;</div>
                <div
                    className={cn('rightBtn')}
                    onClick={() => onCurrentIdChange(currentId + 1)}
                >&#62;</div>
            </div>
        );
    }
};
