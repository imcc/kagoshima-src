import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';

// Component
import LightBox from './LightBox';

// SCSS
import styles from '../../assets/css/kagoshima/component/_LoadingItem.scss';

export default class ImagesLightBox extends React.Component {
    static defaultProps = {
        visible: false,
        offscreen: true,
    };

    static propTypes = {
        visible: PropTypes.bool.isRequired,
        offscreen: PropTypes.bool.isRequired,
        onOffscreenChange: PropTypes.func.isRequired,
    };

    onTransitionEndHandle () {
        const {visible, offscreen, onOffscreenChange} = this.props;

        if (!visible && !offscreen) {
            onOffscreenChange(true);
        }
    }

    render() {
        const cn = classnames.bind(styles);
        const {visible, offscreen} = this.props;

        return (
            <div
                className={cn('main')}
                data-visible={visible}
                data-offscreen={offscreen}
                onTransitionEnd={() => this.onTransitionEndHandle()}
            >
                <div>
                    <svg version="1.1"
                        baseProfile="full"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <circle cx="50%" cy="50%" r="10px" />
                        <circle cx="50%" cy="50%" r="10px" />
                        <circle cx="50%" cy="50%" r="10px" />
                    </svg>
                </div>
            </div>
        );
    }
};
