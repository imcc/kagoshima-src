import React from 'react'
import classnames from 'classnames/bind';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

// Component
import HScrollingItem from './HScrollingItem';
import ScrollIndicator from './ScrollIndicator';

// SCSS
import styles from '../../assets/css/kagoshima/component/_SectionIntro.scss';

export default class SectionIntro extends React.Component {
    static defaultProps = {
        loaded: false,
    };

    static propTypes = {
        loaded: PropTypes.bool.isRequired,
    };
z
    render() {
        const cn = classnames.bind(styles);
        const {
            currentId, scrollOffset, onCurrentIdChange, onScrollOffsetChange,
            loaded,
        } = this.props;
        const scrollItemCount = 3;

        return (
            <section className={classnames(cn('main'), loaded ? 'loaded' : 'ready-to-load')} id="intro">
                <h1>簡介</h1>
                <div>
                    <div>
                        <HScrollingItem
                            currentId={currentId}
                            onCurrentIdChange={currentId => onCurrentIdChange(currentId)}
                            scrollOffset={scrollOffset}
                            onScrollOffsetChange={scrollOffset => onScrollOffsetChange(scrollOffset)}
                            autoScrollTime={10000}
                        >
                            <div className={cn('scrollItem')}>
                                <aside>
                                    <iframe key="iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d435334.7112530743!2d130.27797735202634!3d31.52271986322668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x353e615200e3c53d%3A0x9adcfdad5d5c5885!2z5pel5pys6bm_5YWS5bO257ij6bm_5YWS5bO25biC!5e0!3m2!1szh-TW!2stw!4v1505007898761" frameBorder="0" style={{border: 0}} allowFullScreen></iframe>
                                </aside>
                                <article>
                                    鹿兒島縣位於日本九州島的最南端，除了九州島的部分，還包括薩摩半島和大隅半島，以及薩南群島。古屬薩摩國以及大隅國，鹿兒島市為縣政府所在地。<br/><br/>
                                    鹿兒島縣總面積9187平方公里，海岸線長2,722公里，共有島嶼605個。該縣由19個市、20個町和4個村共43個市町村組成。鹿兒島縣分佈著許多火山，約一半的土地被火山灰所覆蓋。
                                    <a href="https://zh.wikipedia.org/wiki/%E9%B9%BF%E5%84%BF%E5%B2%9B%E5%8E%BF">維基百科</a>
                                </article>
                            </div>
                            <div className={cn('scrollItem')}>
                                <aside>
                                    <Link to="/gallery/0" className={classnames('blockedAnchor')}>
                                        <div className={cn('asideImage')} data-image="volcano"></div>
                                    </Link>
                                </aside>
                                <article>
                                    櫻島，位在日本九州鹿兒島縣的鹿兒島灣中，是鹿兒島的象徵代表，也是一座活躍的複式火山，火山口距離有50萬人居住的鹿兒島市市區僅8公里距離。 櫻島被劃入霧島錦江灣國立公園的範圍，也被國際火山學與地球內部化學協會列為「十年火山」之一。
                                    <a href="https://zh.wikipedia.org/zh-tw/%E6%AB%BB%E5%B3%B6">維基百科</a>
                                </article>
                            </div>
                            <div className={cn('scrollItem')}>
                                <aside>
                                    <Link to="/gallery/1" className={classnames('blockedAnchor')}>
                                        <div className={cn('asideImage')} data-image="jingu"></div>
                                    </Link>
                                </aside>
                                <article>
                                    鹿兒島最著名的宗教景點之一就是霧島神宮，宮內祭祀著日本開國神話天孫降臨的主角-瓊瓊杵尊，是目前南九州最大的神宮。<br/><br/>
                                    天孫降臨是指神道中的太陽女神天照大御神的孫子瓊瓊杵尊，從高天原降臨葦原中國(日本)。降臨之時天照大御神授三神器與他約定世代統治日本。
                                    <a href="https://zh.wikipedia.org/wiki/%E5%A4%A9%E5%AD%AB%E9%99%8D%E8%87%A8">維基百科</a>
                                </article>
                            </div>
                        </HScrollingItem>
                    </div>
                    <div>
                        <div>
                            <ScrollIndicator
                                count={scrollItemCount}
                                currentId={currentId >= scrollItemCount ? 0 : (currentId < 0 ? (scrollItemCount - (Math.abs(currentId) % scrollItemCount)) % scrollItemCount : currentId)}
                                onCurrentIdChange={currentId => onCurrentIdChange(currentId)}
                            />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
};
