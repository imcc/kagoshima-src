import React from 'react'
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';

// SCSS
import styles from "../../assets/css/kagoshima/component/_HScrollingItem.scss";

export default class HScrollingItem extends React.Component {
    static defaultProps = {
        children: [],
        currentId: 0,
        scrollOffset: 0,
        autoScrollTime: 0, // In ms
        instantScroll: false,
        onInstantScrollChange: () => {}
    };

    static propTypes = {
        currentId: PropTypes.number.isRequired,
        scrollOffset: PropTypes.number.isRequired,
        autoScrollTime: PropTypes.number,
        instantScroll: PropTypes.bool.isRequired,
        onCurrentIdChange: PropTypes.func.isRequired,
        onScrollOffsetChange: PropTypes.func.isRequired,
        onInstantScrollChange: PropTypes.func.isRequired,
    };

    touchStartX = null;
    itemWidth = 0;
    scrollSensitivity = 20;
    positionReseting = false;
    scrollInterval = null;

    onTouchStartHandle (e) {
        const {children} = this.props;
        const len = children.length;

        if (this.positionReseting) {
            return;
        }

        this.touchStartX = e.changedTouches[0].pageX;
    }

    onTouchMoveHandle (e) {
        if (this.touchStartX === null) {
            return;
        }

        let offset = (e.changedTouches[0].pageX - this.touchStartX) / this.itemWidth * 100;

        offset = Math.abs(offset) >= 100 ? 100 * (offset > 0 ? 1 : -1) : +offset.toFixed(1);

        this.props.onScrollOffsetChange(offset);
    }

    onTouchEndHandle (e) {
        if (this.touchStartX === null) {
            return;
        }

        const {children, currentId, onCurrentIdChange, scrollOffset, onScrollOffsetChange} = this.props;
        const len = children.length;

        this.touchStartX = null;

        if (Math.abs(scrollOffset) >= this.scrollSensitivity) {
            let nextId = currentId + (scrollOffset > 0 ? -1 : 1);

            if (nextId < -1) {
                nextId += len;
            }

            if (nextId > len) {
                nextId %= len;
            }

            onCurrentIdChange(nextId);
        }

        onScrollOffsetChange(0);
    }

    componentDidMount () {
        this.itemWidth = ReactDOM.findDOMNode(this).clientWidth;
        this.setupScrollInterval();
    }

    componentDidUpdate (prevProps) {
        const {currentId, onCurrentIdChange, children, autoScrollTime, loaded} = this.props;
        const {wrap} = this.refs;
        const cLen = children.length;

        if (currentId === 0 || currentId === cLen - 1) {
            this.positionReseting = false;
        }

        if (currentId === children.length) {
            this.positionReseting = true;

            setTimeout(() => {
                wrap.setAttribute('data-pause', 'true');
                wrap.style.transform = 'translateX(-100%)';

                setTimeout(() => {
                    wrap.setAttribute('data-pause', '');
                    onCurrentIdChange(0);
                }, 50);
            }, 650);
        }

        if (currentId === -1) {
            this.positionReseting = true;

            setTimeout(() => {
                wrap.setAttribute('data-pause', 'true');
                wrap.style.transform = `translateX(${(cLen) * -100}%)`;

                setTimeout(() => {
                    wrap.setAttribute('data-pause', '');
                    onCurrentIdChange(cLen - 1);
                }, 50);
            }, 650);
        }

        if (prevProps.autoScrollTime !== autoScrollTime) {
            this.clearScrollInterval();

            if (autoScrollTime) {
                this.setupScrollInterval();
            }
        }
    }

    clearScrollInterval () {
        if (this.scrollInterval !== null) {
            clearInterval(this.scrollInterval);
            this.scrollInterval = null;
        }
    }

    setupScrollInterval () {
        const {autoScrollTime, onCurrentIdChange} = this.props;

        if (autoScrollTime <= 0) {
            return;
        }

        this.scrollInterval = setInterval(() => {
            const {currentId} = this.props;

            onCurrentIdChange(currentId + 1)
        }, autoScrollTime);
    }

    render() {
        const cn = classnames.bind(styles);
        const {
            children, currentId, scrollOffset, expand, onScrollOffsetChange,
            instantScroll, onInstantScrollChange,
        } = this.props;
        const itemLen = children.length;
        const selfCurrentId = currentId < 0 ? itemLen - (Math.abs(currentId) % itemLen) : currentId;
        const remain = selfCurrentId % itemLen;
        const selfItems = [
            children[itemLen - 1],
            ...children,
            children[0],
        ];
        let showItems = [];

        if (selfCurrentId % itemLen === 0) {
            showItems.push(- 1, 0, 1);
        } else if (selfCurrentId % itemLen === itemLen - 1) {
            showItems.push(itemLen - 2, itemLen - 1, itemLen);
        } else {
            showItems.push(remain - 1, remain, remain + 1);
        }

        const itemsEl = selfItems.map((child, idx) => {
            const selfIdx = -1 + idx;
            const showItem = showItems.includes(selfIdx);

            return (
                <li key={idx}>
                    {child}
                </li>
            );
        });

        return (
            <div
                className={cn('main')}
                data-expand={expand}
                onTouchStart={e => this.onTouchStartHandle(e)}
                onTouchMove={e => this.onTouchMoveHandle(e)}
                onTouchEnd={e => this.onTouchEndHandle(e)}
            >
                <ul
                    data-instantscroll={instantScroll}
                    style={{
                        transform: `translateX(${(currentId + 1) * -100 + scrollOffset}%)`,
                    }}
                    ref="wrap"
                >{itemsEl}</ul>
            </div>
        );
    }
};
