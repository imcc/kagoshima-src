import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import {withRouter} from 'react-router-dom';

// Component
import LightBox from './LightBox';

// SCSS
import styles from '../../assets/css/kagoshima/component/_ImagesLightBox.scss';

// Helper
import {staticScrollTo, setAppShowModal} from '../helper/scroll';

class ImagesLightBox extends React.Component {
    static defaultProps = {
        preloaded: false,
        data: [],
    };

    static propTypes = {
        preloaded: PropTypes.bool.isRequired,
        data: PropTypes.array.isRequired,
    };

    componentDidMount () {
        const {onVisibleChange, onOffscreenChange} = this.props;

        setTimeout(() => onVisibleChange(true), 50);
    }

    componentDidUpdate (prevProps) {
        const {visible} = this.props;
        const {pathname, hash} = window.location;
    }

    render() {
        const cn = classnames.bind(styles);
        const {
            currentId, scrollOffset, visible, instantScroll,
            onCurrentIdChange, onScrollOffsetChange, onVisibleChange,
            onInstantScrollChange, preloaded, data, history,
        } = this.props;

        const imageDivs = data.map((item, idx) => {
            return (
                <div
                    key={idx}
                    className={cn('scrollItem')}
                    data-id={idx}
                ></div>
            );
        });

        return (
            <div className={cn('main', {preloaded})}>
                <LightBox
                    currentId={currentId}
                    onCurrentIdChange={currentId => onCurrentIdChange(currentId)}
                    scrollOffset={scrollOffset}
                    onScrollOffsetChange={scrollOffset => onScrollOffsetChange(scrollOffset)}
                    visible={visible}
                    onVisibleChange={visible => onVisibleChange(visible)}
                    instantScroll={instantScroll}
                    onInstantScrollChange={instantScroll => onInstantScrollChange(instantScroll)}
                    onClose={() => {
                        const pathname = [0, 1].includes(+currentId) ? 'intro' : 'gallery';

                        setAppShowModal(false);
                        history.replace(`/${pathname}`);
                    }}
                >
                    {imageDivs}
                </LightBox>
            </div>
        );
    }
};

export default withRouter(ImagesLightBox);
