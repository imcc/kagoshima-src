import React from 'react'
import classnames from 'classnames/bind';

// Component
import ShiftingSquares from './ShiftingSquares';

// SCSS

import styles from '../../assets/css/kagoshima/component/_SectionFooter.scss';

export default class SectionFooter extends React.Component {
    render() {
        const cn = classnames.bind(styles);

        return (
            <footer className={cn('main')}>
                <div>
                    <span>David Huang's Demo Page</span>
                    <span>2017 Summer</span>
                </div>
            </footer>
        );
    }
};
