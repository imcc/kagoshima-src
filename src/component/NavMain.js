import React from 'react'
import classnames from 'classnames/bind';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

// SCSS
import styles from '../../assets/css/kagoshima/component/_NavMain.scss';

export default class NavMain extends React.Component {
    static defaultProps = {
        items: [],
        currentId: 0,
        mini: false,
    };

    static propTypes = {
        items: PropTypes.array.isRequired,
        currentId: PropTypes.number.isRequired,
        mini: PropTypes.bool.isRequired,
    };

    render() {
        const cn = classnames.bind(styles);
        const {items, currentId, onCurrentIdChange, mini} = this.props;
        const itemsEl = items.map((data, idx) => {
            const {key, text} = data;

            return (
                <li
                    key={key}
                >
                    <NavLink
                        to={`/${key}`}
                        className={cn('navLink', {mini})}
                        activeClassName={cn('navLink', 'selected', {mini})}
                    >{text}</NavLink>
                </li>
            );
        });

        return (
            <nav className={cn('main')}>
                <ul>{itemsEl}</ul>
            </nav>
        );
    }
};
