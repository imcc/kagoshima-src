import React from 'react'
import classnames from 'classnames/bind';

// Component
import HScrollingItem from './HScrollingItem';
import ScrollIndicator from './ScrollIndicator';

// SCSS

import styles from '../../assets/css/kagoshima/component/_ShiftingSquares.scss';

export default class ShiftingSquares extends React.Component {
    constructor (props) {
        super(props);
    }

    render() {
        const cn = classnames.bind(styles);

        return (
            <ul className={cn('main')}>
                <li>JS</li>
                <li>CSS3</li>
                <li>HTML5</li>
                <li>Git</li>
            </ul>
        );
    }
};
