import React from 'react'
import classnames from 'classnames/bind';
import PropTypes from 'prop-types';

// Component
import NavMain from './NavMain';
import MenuListButton from './MenuListButton';
import FullscreenMenuList from './FullscreenMenuList';

// SCSS
import styles from "../../assets/css/kagoshima/component/_SectionNav.scss";

// Config
import {navItems} from '../config/mainNav';

// Helper
import {whichSection} from '../helper/scroll';

// Constant
import {SCROLL_VISIBILITY_CHANGE_INTERVAL as visibleChangeInterval} from '../constant/SectionNav';

let scrollTimer = null;
let scrolling = false;

export default class SectionNav extends React.Component {
    static defaultProps = {
        mini: false,
        visible: true,
        offscreen: false,
        fullScrListVisible: false,
    };

    static propTypes = {
        mini: PropTypes.bool.isRequired,
        onMiniChange: PropTypes.func.isRequired,
        onVisibleChange: PropTypes.func.isRequired,
        onOffscreenChange: PropTypes.func.isRequired,
        onShowFullscreenList: PropTypes.func.isRequired,
    };

    scrolled = false;

    onTransitionEndHandle () {
        const {visible, offscreen, onOffscreenChange} = this.props;

        if (!visible && !offscreen) {
            return onOffscreenChange(true);
        }
    }

    shouldHideNav () {
        const initScrollTop = window.scrollY;

        if (!this.scrolled) {
            this.scrolled = true;

            const async = (resolve, reject) => {
                setTimeout(() => {
                    resolve(window.scrollY - initScrollTop > 0);
                    this.scrolled = false;
                }, visibleChangeInterval);
            };

            return new Promise(async);
        } else {
            return false;
        }
    }

    render() {
        const cn = classnames.bind(styles);
        const {currentId, onCurrentIdChange, fullScrListVisible, onShowFullscreenList, mini, visible, offscreen} = this.props;

        return (
            <div
                className={classnames(visible ? 'visible' : 'invisible', offscreen ? 'offscreen' : 'onscreen', cn('main'))}
                onTransitionEnd={() => this.onTransitionEndHandle()}
            >
                <div className={cn('fullscreenNav')}>
                    <MenuListButton checked={fullScrListVisible} onClick={e => onShowFullscreenList(!fullScrListVisible)}/>
                    <span>鹿兒島初夏紀行</span>
                </div>
                <div className={cn('navBar')}>
                    <NavMain items={navItems} currentId={currentId} onCurrentIdChange={idx => onCurrentIdChange(idx)} mini={mini} />
                </div>
            </div>
        );
    }
};
