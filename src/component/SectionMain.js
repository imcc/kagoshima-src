import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import {Link} from 'react-router-dom';
import {staticScrollTo} from '../helper/scroll';

// Component
import NavMain from './NavMain';

// SCSS
import styles from '../../assets/css/kagoshima/component/_SectionMain.scss';

// Image
import titleTextSVG from '../../assets/images/SectionMain/title.svg';

export default class SectionMain extends React.Component {
    render() {
        const cn = classnames.bind(styles);
        const {loaded} = this.props;

        return (
            <section className={classnames(cn('main'), loaded ? 'loaded' : 'ready-to-load')} id="main">
                <header>
                    <img src={titleTextSVG} alt="鹿兒島初夏紀行"/>
                    <div onClick={() => staticScrollTo('intro')}>
                        <Link to="/intro" />
                    </div>
                </header>
            </section>
        );
    }
};
