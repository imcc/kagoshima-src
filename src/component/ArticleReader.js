import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import {withRouter} from 'react-router-dom';
import {staticScrollTo} from '../helper/scroll';

// SCSS
import styles from "../../assets/css/kagoshima/component/_ArticleReader.scss";

// Article
import articles from '../config/articles';

// Helper
import {setAppShowModal} from '../helper/scroll';

class ArticleReader extends React.Component {
    static defaultProps = {
        currentId: 0,
    };

    static propTypes = {
        currentId: PropTypes.number.isRequired,
        history: PropTypes.object.isRequired,
    };

    constructor (props) {
        super(props);
        setAppShowModal(true);
    }

    state = {
        mount: false,
    };

    componentDidMount () {
        setTimeout(() => this.setState({mount: true}), 50);
    }

    onTransitionEndHandle () {
        const {history} = this.props;
        const {mount} = this.state;
        const {pathname} = history.location;
        let lastHistory = '';

        if (!mount) {
            parent = pathname.slice(0, pathname.lastIndexOf('/'));
            history.replace(parent);
        }
    }

    onClose () {
        setAppShowModal(false);
        this.setState({mount: false});
    }

    render () {
        const cn = classnames.bind(styles);
        const {currentId} = this.props;
        const {mount} = this.state;
        const {image, title, text} = articles[currentId];
        const content = text.split('\n').map((item, idx) => {
            return (
                <p key={idx}>{item}</p>
            );
        });

        return (
            <div
                className={cn('main', {mount})}
                onTransitionEnd={() => this.onTransitionEndHandle()}
            >
                <div
                    className={cn('closeBtn')}
                    onClick={() => this.onClose()}
                >✕</div>
                <article>
                    <header>
                        <h1>{title}</h1>
                        <h4>2017-09-15 | 黃耀宏</h4>
                    </header>
                    <section>
                        <figure>
                            <img srcSet={`${image.smartPhone} 480w, ${image.tablet} 768w`}
                                src={image.desktop}
                                alt={title}
                            />
                        </figure>
                        {content}
                    </section>
                </article>
            </div>
        );
    }
};

export default withRouter(ArticleReader);
