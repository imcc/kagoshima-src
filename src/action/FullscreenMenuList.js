// Actions
import * as TYPES from '../constant/FullscreenMenuList';

export const updateVisible = visible => {
    return {
        type: TYPES.UPDATE_VISIBLE,
        visible
    };
};

export const updateOffscreen = offscreen => {
    return {
        type: TYPES.UPDATE_OFFSCREEN,
        offscreen
    };
};
