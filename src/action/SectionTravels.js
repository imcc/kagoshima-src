// Actions
import * as TYPES from '../constant/SectionTravels';

export const updateLoaded = loaded => {
    return {
        type: TYPES.UPDATE_LOADED,
        loaded
    };
};
