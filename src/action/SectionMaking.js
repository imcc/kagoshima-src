// Actions
import * as TYPES from '../constant/SectionMaking';

export const updateLoaded = loaded => {
    return {
        type: TYPES.UPDATE_LOADED,
        loaded
    };
};
