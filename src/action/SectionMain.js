// Actions
import * as TYPES from '../constant/SectionMain';

export const updateLoaded = loaded => {
    return {
        type: TYPES.UPDATE_LOADED,
        loaded
    };
};
