// Actions
import * as TYPES from '../constant/SectionAboutMe';

export const updateLoaded = loaded => {
    return {
        type: TYPES.UPDATE_LOADED,
        loaded
    };
};
