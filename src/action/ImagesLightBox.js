// Actions
import * as TYPES from '../constant/ImageLightBox';

export const updateCurrentId = currentId => {
    return {
        type: TYPES.UPDATE_CURRENT_ID,
        currentId
    };
};

export const updateScrollOffset = scrollOffset => {
    return {
        type: TYPES.UPDATE_SCROLL_OFFSET,
        scrollOffset
    };
};

export const updateVisible = visible => {
    return {
        type: TYPES.UPDATE_VISIBLE,
        visible
    };
};

export const updateOffscreen = offscreen => {
    return {
        type: TYPES.UPDATE_OFFSCREEN,
        offscreen
    };
};

export const updateInstantScroll = instantScroll => {
    return {
        type: TYPES.UPDATE_INSTANT_SCROLL,
        instantScroll
    };
};

export const updatePreloaded = preloaded => {
    return {
        type: TYPES.UPDATE_PRELOADED,
        preloaded
    };
};
