// Actions
import * as TYPES from '../constant/App';

export const updateInitialized = initialized => {
    return {
        type: TYPES.UPDATE_INITIALIZED,
        initialized
    };
};
