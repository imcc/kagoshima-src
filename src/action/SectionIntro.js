// Actions
import * as TYPES from '../constant/SectionIntro';

export const updateCurrentId = currentId => {
    return {
        type: TYPES.UPDATE_CURRENT_ID,
        currentId
    };
};

export const updateScrollOffset = scrollOffset => {
    return {
        type: TYPES.UPDATE_SCROLL_OFFSET,
        scrollOffset
    };
};

export const updateLoaded = loaded => {
    return {
        type: TYPES.UPDATE_LOADED,
        loaded
    };
};
