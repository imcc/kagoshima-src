// Actions
import * as TYPES from '../constant/SectionNav';

export const updateCurrentId = currentId => {
    return {
        type: TYPES.UPDATE_CURRENT_ID,
        currentId
    };
};

export const updateMini = mini => {
    return {
        type: TYPES.UPDATE_MINI,
        mini
    };
};

export const updateVisible = visible => {
    return {
        type: TYPES.UPDATE_VISIBLE,
        visible
    };
};

export const updateOffscreen = offscreen => {
    return {
        type: TYPES.UPDATE_OFFSCREEN,
        offscreen
    };
};
