// Actions
import * as TYPES from '../constant/SectionGallery';

export const updateLoaded = loaded => {
    return {
        type: TYPES.UPDATE_LOADED,
        loaded
    };
};
