import sections from '../config/sections';

export const scrollTo = scrollEnd => {
    let start = window.scrollY;
    let currentPos = start;
    const direction = scrollEnd - start >= 0 ? 1 : -1;
    let incrementer = 0.01;
    let id = null;

    const animate = () => {
        incrementer += .0002;

        currentPos += (1 / incrementer) * direction;
        window.scroll(0, currentPos);

        if (direction > 0 && currentPos > scrollEnd || direction < 0 && currentPos < scrollEnd) {
            window.scrollY = scrollEnd;
            window.cancelAnimationFrame(id);
            return;
        }

        id = window.requestAnimationFrame(animate);
    }

    animate();
};

export const scrollToSection = id => {
    const target = document.getElementById('section' + id);

    if (target) {
        scrollTo(target.offsetTop);
    }
};

export const staticScrollTo = (id, offset = 36) => {
    const target = document.getElementById(id);

    if (!target) return;

    const {top} = target.getBoundingClientRect();
    window.scroll(0, window.scrollY + top - offset);
};

export const whichSection = () => {
    const sy = window.scrollY;
    const {abs} = Math;
    const wh = window.innerHeight;
    const tolerance = wh * 0.9;
    const sectionsTop = Object.keys(sections).map((id, idx) => {
        const {top, height, bottom} = document.getElementById(id).getBoundingClientRect();
        return {id, top, bottom, height, idx}
    });

    let currentSection = -1;

    for(let i = 0; i < sectionsTop.length; i += 1) {
        const {top, height, bottom} = sectionsTop[i];

        if (top >= 0 && top <= height * 0.8) {
            currentSection = i;
            break;
        }
    }

    return currentSection;
};

export const setAppShowModal = (showModal) => {
    document.body.classList[showModal ? 'add' : 'remove']('show-modal');
};
