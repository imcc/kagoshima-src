import sectionConfig from '../config/sections';

export const updateURLBySectionId = (() => {
    let sectionsId = null;

    return (id, history) => {
        if (sectionsId === null) {
            sectionsId = Object.keys(sectionConfig);
        }

        if (id < 0) return;

        const pathname = sectionsId[id];

        if (!pathname || history.location.pathname === '/' + pathname) return;
        history.replace(pathname);
    };
})();

export default {updateURLBySectionId};
