export const preloadImages = imageList => {
    const loadImage = path => {
        const async = (resolve, reject) => {
            const img = new Image();

            img.onload = e => {
                resolve(e);
            };
            img.src = path;
        };

        return new Promise(async);
    };
    const preloadFunc = imageList.map(path => {
        return loadImage(path);
    });

    return Promise.all(preloadFunc);
};
