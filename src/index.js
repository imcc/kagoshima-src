// static imports
import '../assets/css/kagoshima/core.scss';

// javascript
import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Switch, Route, Redirect} from 'react-router-dom';

import sectionsConfig from './config/sections';

import App from './App';

const root = document.getElementById('kagoshima');

ReactDOM.render((
    <HashRouter>
          <Switch>
              <Route
                  path="/:section"
                  render={({match}) => {
                      const isValidSection = (section) => {
                          return Object.keys(sectionsConfig).includes(section);
                      };

                      return isValidSection(match.params.section) ?
                          (<App />) :
                          (<Redirect to="/main" />);
                  }}
              />
              <Route>
                   <Redirect to="/main" />
              </Route>
          </Switch>
    </HashRouter>
), root);
