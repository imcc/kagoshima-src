import {connect} from 'react-redux';

// Action
import * as actions from '../action/ImagesLightBox';

// Component
import ImagesLightBox from '../component/ImagesLightBox';

const mapStateToProps = state => {
    const {scrollOffset, visible, offscreen, instantScroll, preloaded} = state.ImagesLightBox;

    return {scrollOffset, visible, offscreen, instantScroll, preloaded};
};

const mapDispatchToProps = dispatch => {
    return {
        onScrollOffsetChange: scrollOffset => dispatch(actions.updateScrollOffset(scrollOffset)),
        onVisibleChange: visible => dispatch(actions.updateVisible(visible)),
        onOffscreenChange: offscreen => dispatch(actions.updateOffscreen(offscreen)),
        onInstantScrollChange: instantScroll => dispatch(actions.updateInstantScroll(instantScroll)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ImagesLightBox);
