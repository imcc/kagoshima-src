import {connect} from 'react-redux';

// Action
import * as actions from '../action/SectionIntro';
import * as imagesLightBoxActions from '../action/ImagesLightBox';

// Component
import SectionIntro from '../component/SectionIntro';

const mapStateToProps = state => {
    const {currentId, scrollOffset, loaded,} = state.SectionIntro;

    return {currentId, scrollOffset, loaded,};
};

const mapDispatchToProps = dispatch => {
    return {
        onCurrentIdChange: currentId => dispatch(actions.updateCurrentId(currentId)),
        onScrollOffsetChange: scrollOffset => dispatch(actions.updateScrollOffset(scrollOffset)),
        onLightBoxShow: lightBoxItemIdx => {
            dispatch(imagesLightBoxActions.updateInstantScroll(true));
            dispatch(imagesLightBoxActions.updateCurrentId(lightBoxItemIdx));
            dispatch(imagesLightBoxActions.updateOffscreen(false));
            dispatch(imagesLightBoxActions.updateVisible(true));

            setTimeout(() => {
                dispatch(imagesLightBoxActions.updateInstantScroll(false));
            }, 250);
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SectionIntro);
