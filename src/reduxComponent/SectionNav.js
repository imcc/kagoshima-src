import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

// Action
import * as actions from '../action/SectionNav';
import * as fullscrListActions from '../action/FullscreenMenuList';

// Component
import SectionNav from '../component/SectionNav';

const mapStateToProps = state => {
    const {mini, visible, offscreen} = state.SectionNav;

    return {mini, visible, offscreen, fullScrListVisible: state.FullscreenMenuList.visible};
};

const mapDispatchToProps = dispatch => {
    return {
        onShowFullscreenList: visible => {
            if (visible) {
                dispatch(fullscrListActions.updateOffscreen(false));
            }

            dispatch(fullscrListActions.updateVisible(visible));
        },
        onMiniChange: mini => dispatch(actions.updateMini(mini)),
        onVisibleChange: visible => dispatch(actions.updateVisible(visible)),
        onOffscreenChange: offscreen => dispatch(actions.updateOffscreen(offscreen)),
    };
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps,
)(SectionNav));
