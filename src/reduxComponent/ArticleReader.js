import {connect} from 'react-redux';

// Action
import * as actions from '../action/ArticleReader';

// Component
import ArticleReader from '../component/ArticleReader';

const mapStateToProps = state => {
    const {currentId, visible, offscreen} = state.ArticleReader;

    return {currentId, visible, offscreen};
};

const mapDispatchToProps = dispatch => {
    return {
        onVisibleChange: visible => dispatch(actions.updateVisible(visible)),
        onOffscreenChange: offscreen => dispatch(actions.updateOffscreen(offscreen)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ArticleReader);
