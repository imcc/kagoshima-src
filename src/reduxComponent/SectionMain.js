import {connect} from 'react-redux';

// Action
import * as sectionNavActions from '../action/SectionNav';

// Helper
import {staticScrollTo} from '../helper/scroll';

// Component
import SectionMain from '../component/SectionMain';

const mapStateToProps = state => {
    const {loaded} = state.SectionMain;

    return {loaded};
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SectionMain);
