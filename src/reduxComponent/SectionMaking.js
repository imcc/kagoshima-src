import {connect} from 'react-redux';

// Component
import SectionMaking from '../component/SectionMaking';

const mapStateToProps = state => {
    const {loaded} = state.SectionMaking;

    return {loaded};
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SectionMaking);
