import {connect} from 'react-redux';

// Component
import SectionAboutMe from '../component/SectionAboutMe';

const mapStateToProps = state => {
    const {loaded} = state.SectionAboutMe;

    return {loaded};
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SectionAboutMe);
