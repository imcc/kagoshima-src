import {connect} from 'react-redux';

// Action
import * as actions from '../action/FullscreenMenuList';

// Component
import FullscreenMenuList from '../component/FullscreenMenuList';

const mapStateToProps = state => {
    const {visible, offscreen} = state.FullscreenMenuList;

    return {visible, offscreen};
};

const mapDispatchToProps = dispatch => {
    return {
        onVisibleChange: visible => dispatch(actions.updateVisible(visible)),
        onOffscreenChange: offscreen => dispatch(actions.updateOffscreen(offscreen)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(FullscreenMenuList);
