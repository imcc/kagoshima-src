import {connect} from 'react-redux';

// Action
import * as articleReaderActions from '../action/ArticleReader';

// Component
import SectionTravels from '../component/SectionTravels';

const mapStateToProps = state => {
    const {loaded} = state.SectionTravels;
    return {loaded};
};

const mapDispatchToProps = dispatch => {
    return {
        onShowArticleReader: currentId => {
            dispatch(articleReaderActions.updateCurrentId(currentId));
            dispatch(articleReaderActions.updateOffscreen(false));
            dispatch(articleReaderActions.updateVisible(true));
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SectionTravels);
