import {connect} from 'react-redux';

// Action
import * as actions from '../action/ImagesLightBox';

// Component
import SectionGallery from '../component/SectionGallery';

const mapStateToProps = state => {
    const {loaded} = state.SectionGallery;

    return {loaded};
};

const mapDispatchToProps = dispatch => {
    return {
        onLightBoxShow: lightBoxItemIdx => {
            dispatch(actions.updateInstantScroll(true));
            dispatch(actions.updateCurrentId(lightBoxItemIdx));
            dispatch(actions.updateOffscreen(false));
            dispatch(actions.updateVisible(true));

            setTimeout(() => {
                dispatch(actions.updateInstantScroll(false));
            }, 250);
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SectionGallery);
