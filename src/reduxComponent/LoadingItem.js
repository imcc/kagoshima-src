import {connect} from 'react-redux';

// Action
import * as actions from '../action/LoadingItem';

// Component
import LoadingItem from '../component/LoadingItem';

const mapStateToProps = state => {
    const {visible, offscreen} = state.LoadingItem;

    return {visible, offscreen};
};

const mapDispatchToProps = dispatch => {
    return {
        onOffscreenChange: offscreen => dispatch(actions.updateOffscreen(offscreen)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(LoadingItem);
