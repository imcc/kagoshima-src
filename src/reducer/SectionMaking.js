import * as TYPES from '../constant/SectionMaking';

const initState = {
    loaded: false,
};

export default function reducer(state = initState, action) {
    switch (action.type) {
        case TYPES.UPDATE_LOADED:
            return {
                ...state,
                loaded: action.loaded,
            };
        default:
            return state;
    }
};
