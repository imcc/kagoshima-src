import * as TYPES from '../constant/ImageLightBox';

const initState = {
    currentId: 0,
    scrollOffset: 0,
    visible: false,
    offscreen: true,
    instantScroll: false,
    preloaded: false,
};

export default (state = initState, action) => {
    switch (action.type) {
        case TYPES.UPDATE_CURRENT_ID:
            return {
                ...state,
                currentId: action.currentId,
            };
        case TYPES.UPDATE_SCROLL_OFFSET:
            return {
                ...state,
                scrollOffset: action.scrollOffset,
            };
        case TYPES.UPDATE_VISIBLE:
            return {
                ...state,
                visible: action.visible,
            };
        case TYPES.UPDATE_OFFSCREEN:
            return {
                ...state,
                offscreen: action.offscreen,
            };
        case TYPES.UPDATE_INSTANT_SCROLL:
            return {
                ...state,
                instantScroll: action.instantScroll,
            };
        case TYPES.UPDATE_PRELOADED:
            return {
                ...state,
                preloaded: action.preloaded,
            };
        default:
            return state;
    }
};
