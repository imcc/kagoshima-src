import * as TYPES from '../constant/LoadingItem';

const initState = {
    visible: true,
    offscreen: false,
};

export default (state = initState, action) => {
    switch (action.type) {
        case TYPES.UPDATE_VISIBLE:
            return {
                ...state,
                visible: action.visible,
            };
        case TYPES.UPDATE_OFFSCREEN:
            return {
                ...state,
                offscreen: action.offscreen,
            };
        default:
            return state;
    }
};
