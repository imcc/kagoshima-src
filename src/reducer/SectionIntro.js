import * as TYPES from '../constant/SectionIntro';

const initState = {
    currentId: 0,
    scrollOffset: 0,
    loaded: false,
};

export default function reducer(state = initState, action) {
    switch (action.type) {
        case TYPES.UPDATE_CURRENT_ID:
            return {
                ...state,
                currentId: action.currentId,
            };
        case TYPES.UPDATE_SCROLL_OFFSET:
            return {
                ...state,
                scrollOffset: action.scrollOffset,
            };
        case TYPES.UPDATE_LOADED:
            return {
                ...state,
                loaded: action.loaded,
            };
        default:
            return state;
    }
};
