import * as TYPES from '../constant/SectionAboutMe';

const initState = {
    currentId: 0,
    scrollOffset: 0,
    loaded: false,
};

export default function reducer(state = initState, action) {
    switch (action.type) {
        case TYPES.UPDATE_LOADED:
            return {
                ...state,
                loaded: action.loaded,
            };
        default:
            return state;
    }
};
