import * as TYPES from '../constant/FullscreenMenuList';

const initState = {
    visible: false,
    offscreen: true,
};

export default function reducer(state = initState, action) {
    switch (action.type) {
        case TYPES.UPDATE_VISIBLE:
            return {
                ...state,
                visible: action.visible,
            };
        case TYPES.UPDATE_OFFSCREEN:
            return {
                ...state,
                offscreen: action.offscreen,
            };
        default:
            return state;
    }
};
