import {combineReducers} from 'redux';
import ImagesLightBox from './ImagesLightBox';
import SectionNav from './SectionNav';
import SectionMain from './SectionMain';
import SectionIntro from './SectionIntro';
import SectionGallery from './SectionGallery';
import SectionTravels from './SectionTravels';
import SectionMaking from './SectionMaking';
import SectionAboutMe from './SectionAboutMe';
import LoadingItem from './LoadingItem';
import ArticleReader from './ArticleReader';
import FullscreenMenuList from './FullscreenMenuList';
import App from './App';

export default combineReducers({
    ImagesLightBox,
    SectionNav,
    SectionMain,
    SectionIntro,
    SectionGallery,
    SectionTravels,
    SectionMaking,
    SectionAboutMe,
    LoadingItem,
    ArticleReader,
    FullscreenMenuList,
    App,
});
