import * as TYPES from '../constant/ArticleReader';

const initState = {
    currentId: 0,
    visible: false,
    offscreen: true,
};

export default function reducer(state = initState, action) {
    switch (action.type) {
        case TYPES.UPDATE_CURRENT_ID:
            return {
                ...state,
                currentId: action.currentId,
            };
        case TYPES.UPDATE_VISIBLE:
            return {
                ...state,
                visible: action.visible,
            };
        case TYPES.UPDATE_OFFSCREEN:
            return {
                ...state,
                offscreen: action.offscreen,
            };
        default:
            return state;
    }
};
