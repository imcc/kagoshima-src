import * as TYPES from '../constant/App';

const initState = {
    initialized: false,
};

export default (state = initState, action) => {
    switch (action.type) {
        case TYPES.UPDATE_INITIALIZED:
            return {
                ...state,
                initialized: action.initialized,
            };
        default:
            return state;
    }
};
