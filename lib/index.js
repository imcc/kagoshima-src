// css imports
require('../assets/css/kagoshima/core.scss')

// javascript
import React from 'react'
import ReactDOM from 'react-dom'

import App from './App'


const root = document.getElementById('kagoshima')


ReactDOM.render((
  <App/>
), root)
