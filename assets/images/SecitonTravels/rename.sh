#! /bin/bash

for i in `ls -h *.jpg` 
do
	num=${i:0:2}
	
	convert -resize 480x $num-full.jpg $num-480w.jpg
	convert -resize 800x $num-full.jpg $num-800w.jpg
	convert -resize 1920x $num-full.jpg $num-1920w.jpg
done
